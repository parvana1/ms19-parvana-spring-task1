package az.ingress.demo.controller;

import az.ingress.demo.model.dao.Student;
import az.ingress.demo.service.StudentService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")
public class StudentController {
    private final StudentService studentService;


    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }
    @GetMapping("/{id}")
    public void get(@PathVariable Long id){
        studentService.get(id);
    }
    @PostMapping
    public Student create(@RequestBody Student student){
       return  studentService.create(student);
    }
    @PutMapping
    public Student update(@RequestBody Student student){
        return studentService.update(student);
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        studentService.delete(id);
    }
}
