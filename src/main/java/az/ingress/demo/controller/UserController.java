package az.ingress.demo.controller;

import az.ingress.demo.model.User;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    @RequestMapping("/hello")
    public String sayHello(@RequestParam(required = false) String name,@RequestHeader(value="Accept-language") String lang){
     return  switch (lang){
          case "az"  ->"Salam  "+name;
          case "ru"  ->"Privet  "+name;
          case "en"  ->"Hello  "+name;
          default -> "Hello "+ name;
      };


    }

    @PostMapping("/create")
    public User create(@RequestBody User user){
        System.out.println("User created " + user);
        return user;

//        User build=User.builder()
//                .name("Parvana")
//                .surname("Rashidova")
//                .age(29)
//                .build();

    }

    @PutMapping("/update/{id}")
    public User update(@PathVariable Integer id,@RequestBody User user){
        System.out.println("id= "+id+" user updated");
        return user;

    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id){
        System.out.println("id= "+id+" user deleted");

    }
}
