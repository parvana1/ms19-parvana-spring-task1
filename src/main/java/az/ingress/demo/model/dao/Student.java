package az.ingress.demo.model.dao;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Student {
    Long id;
    String name;
    String surname;
    Integer age;

}
