package az.ingress.demo.service;

import az.ingress.demo.model.dao.Student;

public interface StudentService {
  Student get(Long id);
  Student create(Student student);
  Student update(Student student);
  void delete(Long id);


}
