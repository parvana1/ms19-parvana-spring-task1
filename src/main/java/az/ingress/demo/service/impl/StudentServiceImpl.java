package az.ingress.demo.service.impl;

import az.ingress.demo.model.dao.Student;
import az.ingress.demo.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class StudentServiceImpl implements StudentService {


    @Override
    public Student get(Long id) {
        log.info("Student service get method");
        return null;
    }

    @Override
    public Student create(Student student) {
        log.info("Student service create method");
        return null;
    }

    @Override
    public Student update(Student student) {
        log.info("Student service update method");
        return null;
    }

    @Override
    public void delete(Long id) {
        log.info("Student service delete method");

    }
}
